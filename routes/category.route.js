const express = require('express');
const categoryModel = require('../models/category.model');

const router = express.Router();


router.get('/', async (req, res) => {
  const list = await categoryModel.all();
  res.json(list);
})

// router.get('/e', (req, res) => {
//   throw new Error('An error occurred');
// })

// router.get('/', (req, res) => {
//   categoryModel.all().then(rows => {
//     res.json(rows);
//   }).catch(err => {
//     res.json('View error on console.');
//     console.log(err);
//   })
// })

router.get('/:id', async (req, res) => {
  if (isNaN(req.params.id)) {
    return res.status(400).json({
      err: 'Invalid id.'
    });
  }

  const id = +req.params.id || -1;
  const list = await categoryModel.detail(id);
  if (list.length === 0) {
    return res.status(204).end();
  }

  res.json(list[0]);
})

router.post('/', async (req, res) => {
  // const results = await categoryModel.add(req.body);
  // const ret = {
  //   CatID: results.insertId,
  //   ...req.body
  // }
  // res.status(201).json(ret);

  res.status(201).end();
})

module.exports = router;