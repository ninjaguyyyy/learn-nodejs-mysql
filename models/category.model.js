const db = require('../utils/db');

module.exports = {
  all: _ => db.load('select * from categories'),
  detail: id => db.load(`select * from categories where CatID = ${id}`),
};